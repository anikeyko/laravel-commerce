<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\HasPrice;
use App\Models\Country;

class ShippingMethod extends Model
{
    use HasPrice;

    /**
     * Связь со странами
     */
    public function countries()
    {
        return $this->belongsToMany(Country::class);
    }
}
