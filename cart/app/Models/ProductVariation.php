<?php

namespace App\Models;

use App\Cart\Money;
use App\Models\Traits\HasPrice;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProductVariationType;
use App\Models\Product;

class ProductVariation extends Model
{
    use HasPrice;

    /**
     * Тип варианта
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type()
    {
        return $this->hasOne(ProductVariationType::class, 'id', 'product_variation_type_id');
    }

    /**
     * Связь с продуктом
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Связь с запасом, наличие на складе
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    public function inStock()
    {
        return $this->stockCount() > 0;
    }

    public function minStock($count)
    {
        return min($this->stockCount(), $count);
    }

    public function stockCount()
    {
        return $this->stock->sum('pivot.stock');
    }

    public function stock()
    {
        return $this->belongsToMany(
            ProductVariation::class,
            'product_variation_stock_view'
        )->withPivot([
            'stock',
            'in_stock'
        ]);
    }

    /**
     * @param $value
     * @return Money
     */
    public function getPriceAttribute($value)
    {
        if ($value === null) {
            return $this->product->price;
        }
        return new Money($value);
    }

    /**
     * Отличается ли цена от цены продукта
     * @return bool
     */
    public function priceVaries()
    {
        return $this->price->amount() !== $this->product->price->amount();
    }
}
