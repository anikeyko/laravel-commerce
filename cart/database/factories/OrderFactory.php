<?php

use Faker\Generator as Faker;
use App\Models\Order;
use App\Models\User;
use App\Models\Address;
use App\Models\ShippingMethod;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'address_id' => factory(Address::class)->create()->id,
        'shipping_method_id' => factory(ShippingMethod::class)->create()->id,
        // 'user_id' => factory(User::class)->create()->id
    ];
});
