<?php

namespace Tests\Feature\Cart;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use App\Models\ProductVariation;

class CartStoreTest extends TestCase
{
    /**
     * Тестируем то, что корзина недоступна неавторизованным пользователям
     */
    public function test_it_fails_if_unautheticated()
    {
        $this->json('POST', 'api/cart')
            ->assertStatus(401);
    }

    /**
     * Тестируем обязательность наличия продуктов
     */
    public function test_it_requires_products()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/cart')
            ->assertJsonValidationErrors(['products']);
    }

    /**
     * тестируем что список продуктов есть и является массивом
     */
    public function test_it_requires_products_to_be_an_array()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => 1
        ])
            ->assertJsonValidationErrors(['products']);
    }

    /**
     * Тестируем обязательность id у товара
     */
    public function test_it_requires_products_to_have_an_id()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => [
                [
                    'quantity' => 1
                ]
            ]
        ])
            ->assertJsonValidationErrors(['products.0.id']);
    }

    /**
     * Тестируем то, что товар с данным id существует
     */
    public function test_it_requires_products_to_exists()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => [
                [
                    'id' => 1, 'quantity' => 1
                ]
            ]
        ])
            ->assertJsonValidationErrors(['products.0.id']);
    }

    /**
     * Тестируем то, что количество товара является числом
     */
    public function test_it_requires_products_quantity_to_be_numeric()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => [
                [
                    'id' => 1, 'quantity' => 'one'
                ]
            ]
        ])
            ->assertJsonValidationErrors(['products.0.quantity']);
    }

    /**
     * Тестируем то, что минимальное количество товара равно 1
     */
    public function test_it_requires_products_quantity_to_be_at_least_one()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => [
                [
                    'id' => 1, 'quantity' => 0
                ]
            ]
        ])
            ->assertJsonValidationErrors(['products.0.quantity']);
    }

    /**
     * Тестируем то, что при верных данных в БД добавляется строка
     */
    public function test_it_can_add_products_to_the_users_cart()
    {
        $user = factory(User::class)->create();

        $product = factory(ProductVariation::class)->create();

        $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => [
                [
                    'id' => $product->id, 'quantity' => 2
                ]
            ]
        ]);

        $this->assertDatabaseHas('cart_user', [
            'product_variation_id' => $product->id,
            'quantity' => 2
        ]);
    }
}
