<?php

namespace Tests\Feature\Cart;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use App\Models\ProductVariation;

class CartUpdateTest extends TestCase
{

    /**
     * Тестируем то, что корзина недоступна неавторизованным пользователям
     */
    public function test_it_fails_if_unautheticated()
    {
        $this->json('PATCH', 'api/cart/1')
            ->assertStatus(401);
    }

    /**
     * Тестируем ошибку если не найден продукт в корзине
     */
    public function test_it_fails_if_product_cant_be_found()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'PATCH', 'api/cart/1')
            ->assertStatus(404);
    }

    /**
     * Тестируем обязательность количества товара
     */
    public function test_it_requires_a_quantity()
    {
        $user = factory(User::class)->create();

        $product = factory(ProductVariation::class)->create();

        $this->jsonAs($user, 'PATCH', "api/cart/{$product->id}")
            ->assertJsonValidationErrors(['quantity']);
    }

    /**
     * Тестируем то, что количество является числом
     */
    public function test_it_requires_a_numeric_quantity()
    {
        $user = factory(User::class)->create();

        $product = factory(ProductVariation::class)->create();

        $this->jsonAs($user, 'PATCH', "api/cart/{$product->id}", [
            'quantity' => 'one'
        ])
            ->assertJsonValidationErrors(['quantity']);
    }

    /**
     * Тестируем то, что минимальное количество 1 или более
     */
    public function test_it_requires_a_quantity_one_or_more()
    {
        $user = factory(User::class)->create();

        $product = factory(ProductVariation::class)->create();

        $this->jsonAs($user, 'PATCH', "api/cart/{$product->id}", [
            'quantity' => 0
        ])
            ->assertJsonValidationErrors(['quantity']);
    }

    /**
     * Тестируем то, что данные обновились в БД
     */
    public function test_it_updates_the_quantity_of_the_product()
    {
        $user = factory(User::class)->create();

        $user->cart()->attach(
            $product = factory(ProductVariation::class)->create(),
            [
                'quantity' => 1
            ]
        );

        $this->jsonAs($user, 'PATCH', "api/cart/{$product->id}", [
            'quantity' => $quantity = 5
        ]);

        $this->assertDatabaseHas('cart_user', [
            'product_variation_id' => $product->id,
            'quantity' => $quantity
        ]);
    }
}
