<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryIndexTest extends TestCase
{
    /**
     * Проверяет возращается ли список категорий
     */
    public function test_it_returns_a_collection_of_categories()
    {
        $categories = factory(Category::class, 2)->create();

        $this->json('GET', 'api/categories')
            ->assertJsonFragment([
                'slug' => $categories->get(0)->slug
            ], [
                'slug' => $categories->get(1)->slug
            ]);
    }

    /**
     * Проверяет что возвращаются только корневые категории
     */
    public function test_it_returns_only_parent_categories()
    {
        // создаем корневую категорию
        $category = factory(Category::class)->create();

        // добавляем в нее дочернюю
        $category->children()->save(
            factory(Category::class)->create()
        );

        // проверяем
        $this->json('GET', 'api/categories')
            ->assertJsonCount(1, 'data');
    }

    /**
     * Проверяет сортировку
     */
    public function test_it_returns_categories_ordered_by_their_given_order()
    {
        $category = factory(Category::class)->create([
            'order' => 2
        ]);

        $anotherCategory = factory(Category::class)->create([
            'order' => 1
        ]);

        $this->json('GET', 'api/categories')
            ->assertSeeInOrder([
                $anotherCategory->slug, $category->slug
            ]);
    }
}
