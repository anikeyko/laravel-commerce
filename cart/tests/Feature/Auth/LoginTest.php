<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    /**
     * Проверяем обязательность email
     */
    public function test_it_requires_a_email()
    {
        $this->json('POST', 'api/auth/login')
            ->assertJsonValidationErrors(['email']);
    }

    /**
     * Проверяем обязательность пароля
     */
    public function test_it_requires_a_password()
    {
        $this->json('POST', 'api/auth/login')
            ->assertJsonValidationErrors(['password']);
    }

    /**
     * Проверяем ошибки авторизации при неверном пароле
     */
    public function test_it_returns_a_validation_error_if_credentials_dont_match()
    {
        $user = factory(User::class)->create();
        $this->json('POST', 'api/auth/login', [
            'email' => $user->email,
            'password' => 'nope'
        ])
            ->assertJsonValidationErrors(['email']);
    }

    /**
     * Проверяем токен при авторизации с верными данными
     */
    public function test_it_returns_a_validation_error_if_credentials_do_match()
    {
        $user = factory(User::class)->create([
            'password' => 'cats'
        ]);

        $this->json('POST', 'api/auth/login', [
            'email' => $user->email,
            'password' => 'cats'
        ])
            ->assertJsonStructure([
                'meta' => [
                    'token'
                ]
            ]);
    }

    /**
     * Проверяем пользовательские данные при авторизации с верными данными
     */
    public function test_it_returns_a_user_if_credentials_do_match()
    {
        $user = factory(User::class)->create([
            'password' => 'cats'
        ]);

        $this->json('POST', 'api/auth/login', [
            'email' => $user->email,
            'password' => 'cats'
        ])
            ->assertJsonFragment([
                'email' => $user->email
            ]);
    }
}
