<?php

namespace Tests\Unit\Models\ShippingMethods;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\ShippingMethod;
use App\Cart\Money;
use App\Models\Country;

class ShippingMethodTest extends TestCase
{
    public function test_it_returns_a_money_instance_for_the_price()
    {
        $shipping = factory(ShippingMethod::class)->create();

        $this->assertInstanceOf(Money::class, $shipping->price);
    }

    /**
     * Тестируем форматирование цены
     */
    public function test_it_returns_a_formatted_price()
    {
        $shipping = factory(ShippingMethod::class)->create([
            'price' => 1000
        ]);

        $this->assertEquals($shipping->formattedPrice, '10,00 ₽');
    }

    public function test_it_belongs_to_many_countries()
    {
        $shipping = factory(ShippingMethod::class)->create();

        $shipping->countries()->attach(
            factory(Country::class)->create()
        );

        $this->assertInstanceOf(Country::class, $shipping->countries->first());
    }
}
